import json
import pika
import django
import os
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("Received %r" % body)
    contact = json.loads(body)
    send_mail(
        [contact["presenter_email"]],
        "admin@conference.go",
        "Your presentation has been accepted",
        f"{contact['presenter_name']}, we're happy to tell you that your presentation {contact['title']} has been accepted",
        fail_silently=False,
    )


def proccess_rejections(ch, method, properties, body):
    print("Received %r" % body)
    contact = json.loads(body)
    send_mail(
        [contact["presenter_email"]],
        "admin@conference.go",
        "Your presentation",
        f"{contact['presenter_name']}, we're sorry to tell you that your presentation {contact['title']} has been rejected",
        fail_silently=False,
    )


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=proccess_rejections,
    auto_ack=True,
)
channel.start_consuming()
